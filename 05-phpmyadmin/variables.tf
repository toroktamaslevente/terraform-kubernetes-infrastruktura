variable mysql_fqdn {
  type    = string
  default = "mysql.k8s.torok.site"
}