resource kubernetes_namespace phpmyadmin {
  metadata {
    name = "phpmyadmin"
  }
}

# The certificate to use for the HTTPS Ingress
resource kubernetes_manifest phpmyadmin_cert {
  manifest        = {
    apiVersion   = "cert-manager.io/v1alpha2"
    kind          = "Certificate"

    metadata      = {
      name        = "phpmyadmin-cert"
      namespace   = kubernetes_namespace.phpmyadmin.metadata.0.name
    }

    spec          = {
      commonName = var.mysql_fqdn
      secretName = "phpmyadmin-cert"
      dnsNames   = [var.mysql_fqdn]
      issuerRef  = {
        name      = kubernetes_manifest.certman.manifest.metadata.name
        kind      = "ClusterIssuer"
      }
    }
  }
}

# The HTTP Ingress
resource kubernetes_manifest phpmyadmin_ingress {
  manifest              = {
    apiVersion         = "traefik.containo.us/v1alpha1"
    kind                = "IngressRoute"

    metadata            = {
      name              = "phpmyadmin-ingress-https"
      namespace         = kubernetes_namespace.phpmyadmin.metadata.0.name
    }

    spec                = {
      entryPoints      = ["websecure"]
      routes            = [
        {
          match         = "Host(`${var.mysql_fqdn}`)"
          kind          = "Rule"
          services      = [
            {
              name      = "phpmyadmin"
              namespace = "phpmyadmin"
              port      = 80
            }
          ]
          middlewares   = [
            {
              name      = "redirectscheme"
              namespace = "traefik-ingress"
            },
            {
              name      = "compress"
              namespace = "traefik-ingress"
            }
          ]
        }
      ]
      tls = {
        secretName     = kubernetes_manifest.phpmyadmin_cert.manifest.metadata.name
      }
    }
  }
}

resource "helm_release" "phpmyadmin" {
  name             = "phpmyadmin"
  namespace        = kubernetes_namespace.phpmyadmin.metadata.0.name

  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "phpmyadmin"
  version          = "8.2.12"  # chart version, see $ helm repo update / $ helm search repo prometheus-community

  set {
    name = "image.tag"
    value = "5.1.1"  # application version, see $ helm repo update / $ helm search repo prometheus-community
  }
  set {
    name = "db.host"
    value = data.terraform_remote_state.databases.outputs.db_mysql_internal_hostname
  }
  set {
    name = "db.port"
    value = data.terraform_remote_state.databases.outputs.db_mysql_port
  }
}