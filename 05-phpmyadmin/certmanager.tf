resource kubernetes_manifest certman {
  manifest                     = {
    apiVersion                = "cert-manager.io/v1alpha2"
    kind                       = "ClusterIssuer"
    metadata                   = {
      name                     = "letsencrypt"
    }
    spec                       = {
      acme                     = {
        email                  = "toroktamas9804@gmail.com"
        server                 = "https://acme-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name                 = "letsencrypt-account-key"
        }
        solvers                = [
          {
            http01             = {
              ingress          = {
                class          = "traefik-cert-manager"
              }
            }
          }
        ]
      }
    }
  }
}