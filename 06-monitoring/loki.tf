resource helm_release loki_stack {
  name       = "loki-stack"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://grafana.github.io/helm-charts"
  chart      = "loki-stack"

  set {
    name  = "loki.persistence.enabled"
    value = true
  }

  set {
    name  = "promtail.podAnnotations.prometheus\\.io\\/scrape"
    value = "true"
    type  = "string"
  }

  set {
    name  = "promtail.podAnnotations.prometheus\\.io\\/port"
    value = "http-metrics"
  }

  set {
    name  = "loki.config.ruler.storage.type"
    value = "local"
  }

  set {
    name  = "loki.persistence.size"
    value = "5Gi"
  }

  set {
    name  = "loki.config.ruler.storage.local.directory"
    value = "/data/rules"
  }

  set {
    name  = "loki.config.ruler.rule_path"
    value = "/data/scratch"
  }

  set {
    name  = "loki.config.ruler.alertmanager_url"
    value = "http://alertmanager-operated.svc.${kubernetes_namespace.monitoring.metadata.0.name}"
  }

  set {
    name  = "loki.config.ruler.ring.kvstore.store"
    value = "inmemory"
  }

  set {
    name  = "loki.config.ruler.enable_api"
    value = true
  }
}