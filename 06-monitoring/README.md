## Monitoring

This will install the monitoring stack (Prometheus + Grafana + Metrics Server + Loki)
onto the K8S cluster.

Because the monitoring stack is larger, instead of just having one large `main.tf` file,
we are using separate files for each component. In spite of this, the Terraform 
installation still works the usual way.

After installation you should be able to reach Grafana under https://monitoring.k8s.torok.site,
assuming you have setup the DNS record for this domain pointing to the load balancer of the K8S cluster.

### Pre-requisites - Create MySQL user

Connect to PHPMyAdmin and create a MySQL user named `prometheus-exporter` and grant the following rights:
```
CREATE USER 'prometheus-exporter'@'%' IDENTIFIED BY 'XXXXXXXX' WITH MAX_USER_CONNECTIONS 3;
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'prometheus-exporter'@'%';
```


### Change the admin password - once installed

The default password is admin/admin, but you should change that immediately. 
You can reset the admin password by ssh-ing into the Grafana container and then running:
```
# shell into the grafana container of the grafana pod
$ kubectl exec -it [grafana pod name] -c grafana -n monitoring -- ash
# reset the admin password
$ grafana-cli admin reset-admin-password [new_passwd]
```

### Add Prometheus as a data source

Under admin / Data Sources add Prometheus as a data source with url 
"http://prometheus-stack-kube-prom-prometheus:9090" => which is the in-Kubernetes
service url for Prometheus.

### Grafana dashboards to import:

You can import them in Grafana under + / Import - and use the id

MySQL: https://grafana.com/grafana/dashboards/7362
Redis: https://grafana.com/grafana/dashboards/11835
Traefik2: https://grafana.com/grafana/dashboards/10906

### How to use Loki

Grafana should be automatically connected to Loki as a data source.

In Grafana go to Explore, switch to Loki on top, click "Log browser", select label "pod" and then you
can select any pod to look at its logs.


### Notification (alerts) from Grafana

Grafana v8.x can do alerts and those can be hooked up with SMTP, Slack or others.

See example for Slack at https://github.com/grafana/helm-charts/tree/main/charts/grafana#sidecar-for-notifiers