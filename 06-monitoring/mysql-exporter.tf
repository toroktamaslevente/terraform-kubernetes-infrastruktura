resource kubernetes_secret db_scraper_config {
  metadata {
    namespace = kubernetes_namespace.monitoring.metadata.0.name
    name      = "db-scraper-envs"
  }

  data = {
    mysql-password     = var.mysql_user_prometheus_export_psw  # mysql password of the prometheus-exporter user
  }
}

// MySQL Export
// to connect directly to see the collected metrics:
// $ kubectl port-forward -n monitoring svc/mysql-exporter-prometheus-mysql-exporter 9104:9104
// Now you should be able to connect at: http://127.0.0.1:9104/
resource helm_release mysql_data_collector {
  name       = "mysql-exporter"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-mysql-exporter"
  version    = "1.2.2"  # chart version, see $ helm repo update / $ helm search repo prometheus-community

  set {
    name = "image.tag"
    value = "v0.12.1"  # application version, see $ helm repo update / $ helm search repo prometheus-community
  }
  set {
    name  = "mysql.host"
    value = data.terraform_remote_state.databases.outputs.db_mysql_internal_hostname
  }
  set {
    name  = "mysql.port"
    value = data.terraform_remote_state.databases.outputs.db_mysql_port
  }
  set {
    name  = "mysql.user"
    value = "prometheus-exporter"
  }
  set {
    name  = "mysql.existingPasswordSecret.name"
    value = kubernetes_secret.db_scraper_config.metadata.0.name
  }
  set {
    name  = "mysql.existingPasswordSecret.key"
    value = "mysql-password"
  }
  # as this pod uses an "EmptyDir" mount, the cluster-autoscaler would not want to move it to scale down pods,
  # which is why we need to mark it as safe to evict
  # see https://github.com/kubernetes/autoscaler/issues/2184
  set {
    name = "podAnnotations.cluster-autoscaler\\.kubernetes\\.io/safe-to-evict"
    value = "true"
    type = "string"
  }
}