variable prometheus_volume_size {
  type    = string
  default = "5Gi"
}

variable prometheus_replicas {
  type    = number
  default = 1
}

variable alertmanager_replicas {
  type    = number
  default = 1
}

variable monitoring_fqdn {
  type    = string
  default = "monitoring.k8s.torok.site"
}

variable "mysql_user_prometheus_export_psw" {
  description = "The MySQL password of the 'prometheus-exporter' MySQL user"
  type        = string
  sensitive   = true
}