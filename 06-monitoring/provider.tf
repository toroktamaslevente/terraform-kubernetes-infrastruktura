terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = ">= 2.4.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.4.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = ">= 2.0.1"
    }
  }

  backend "s3" {
    bucket                      = "tamas-spaces"
    key                         = "monitoring.tfstate"
    region                      = "fra1"
    skip_region_validation      = true
    endpoint                    = "fra1.digitaloceanspaces.com"
    force_path_style            = true
    skip_credentials_validation = true
  }
}

// this is available ONLY after the K8S cluster has been created
data "terraform_remote_state" "k8s_cluster" {
  backend = "s3"

  config = {
    bucket = "tamas-spaces"
    key    = "k8s_cluster.tfstate"
    region = "fra1"
    skip_region_validation      = true
    endpoint                    = "fra1.digitaloceanspaces.com"
    force_path_style            = true
    skip_credentials_validation = true
  }
}

provider kubernetes {
  host                   = data.terraform_remote_state.k8s_cluster.outputs.cluster_host
  cluster_ca_certificate = base64decode(
    data.terraform_remote_state.k8s_cluster.outputs.cluster_config.cluster_ca_certificate
  )

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "doctl"
    args = ["kubernetes", "cluster", "kubeconfig", "exec-credential",
    "--version=v1beta1", data.terraform_remote_state.k8s_cluster.outputs.cluster_id]
  }

  experiments {
    manifest_resource = true
  }

}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.k8s_cluster.outputs.cluster_host
    cluster_ca_certificate = base64decode(data.terraform_remote_state.k8s_cluster.outputs.cluster_config.cluster_ca_certificate)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "doctl"
      args = ["kubernetes", "cluster", "kubeconfig", "exec-credential",
      "--version=v1beta1", data.terraform_remote_state.k8s_cluster.outputs.cluster_id]
    }
  }
  debug = true
}


data "terraform_remote_state" "databases" {
  backend = "s3"

  config = {
    bucket = "tamas-spaces"
    key    = "databases.tfstate"
    region = "fra1"
    skip_region_validation      = true
    endpoint                    = "fra1.digitaloceanspaces.com"
    force_path_style            = true
    skip_credentials_validation = true
  }
}