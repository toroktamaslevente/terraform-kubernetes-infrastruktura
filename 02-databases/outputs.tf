# MySQL

output db_mysql_cluster_id {
  value = digitalocean_database_cluster.mysql_db.id
}

output db_mysql_internal_hostname {
  value = digitalocean_database_cluster.mysql_db.private_host
}

output db_mysql_port {
  value = digitalocean_database_cluster.mysql_db.port
}

output db_mysql_default_username {
  value = digitalocean_database_cluster.mysql_db.user
}

output db_mysql_default_password {
  value     = digitalocean_database_cluster.mysql_db.password
  sensitive = true
}

output db_mysql_default_database {
  value = digitalocean_database_cluster.mysql_db.database
}
