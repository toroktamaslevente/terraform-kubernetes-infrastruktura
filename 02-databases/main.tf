### MySQL Database

resource digitalocean_database_cluster mysql_db{
  name       = "db-mysql-fra1"
  engine     = "mysql"
  version    = "8"
  size       = "db-s-1vcpu-1gb"
  region     = "fra1"
  node_count = 1
}
