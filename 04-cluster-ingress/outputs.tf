output traefik_namespace {
  value = kubernetes_namespace.traefik_ingress.metadata.0.name
}

output traefik_lb_ip {
  value = digitalocean_loadbalancer.traefik_do_loadbalancer.ip
}
