## Cluster-ingress

This will setup the ingress into the K8S cluster, which includes
setting up the DigitalOcean LoadBalancer, the Traefik instances within
Kubernetes and then pointing the load balancer to Traefik.

It also includes the intallation of the Certificate Manager.


## Installation

NOTE:  
Please note, that the middlewares in `main.tf` can only be created AFTER the Traefik CRDs are created
by the Traefik Helm chart section of `main.tf`.  
For this reason when installing from scratch, first you need to run the `$ tf apply` just with the
Traefik install and can only execute the Middleware creation after, so first you should run 
this Terraform code with the middlewares at the bottom of `main.tf` being commented out and 
then again with the Middlewares included.


### How To connect to the Traefik Dashboard

```
# pick up the Traefik pods
$ kubectl get pods --selector "app.kubernetes.io/name=traefik" -n traefik-ingress --output=name
# port-forward to one of those pods
$ kubectl port-forward [podname] 9000:9000 -n traefik-ingress
# open http://localhost:9000
```
