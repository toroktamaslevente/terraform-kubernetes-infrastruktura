terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = ">= 2.4.0"
    }
  }

  backend "s3" {
    bucket                      = "tamas-spaces"
    key                         = "k8s_cluster.tfstate"
    region                      = "fra1"
    skip_region_validation      = true
    endpoint                    = "fra1.digitaloceanspaces.com"
    force_path_style            = true
    skip_credentials_validation = true
  }
}