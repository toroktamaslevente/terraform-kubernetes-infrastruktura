
output cluster_id {
  value = digitalocean_kubernetes_cluster.k8s_cluster.id
}

output cluster_host {
  value = digitalocean_kubernetes_cluster.k8s_cluster.endpoint
}

output cluster_config {
  value     = digitalocean_kubernetes_cluster.k8s_cluster.kube_config[0]
  sensitive = true
}
