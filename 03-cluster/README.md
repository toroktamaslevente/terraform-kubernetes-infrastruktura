## Cluster

This Terraform module installs the empty Kubernetes cluster on DigitalOcean.

See https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/kubernetes_cluster
