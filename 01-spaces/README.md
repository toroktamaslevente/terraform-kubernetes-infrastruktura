## Spaces

To be able to have a shared Terraform state, we store that state in DigitalOcean's
object storage, called "Spaces".

As this is a pre-requisite to use Terraform, this needs to be done first and this
needs to be setup manually.

### How to setup Spaces in DigitalOcean manually

In the DO UI go to Spaces and create a new Space (if one doesn't already exist)
with name 'tamas-spaces', which means it will be available under:
https://tamas-spaces.fra1.digitaloceanspaces.com
